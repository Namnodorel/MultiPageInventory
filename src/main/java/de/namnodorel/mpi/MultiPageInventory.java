package de.namnodorel.mpi;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class MultiPageInventory {

    protected HashMap<Integer, InventoryPage> pages;
    private String invTitle;
    private Integer currViewingPage;

    private Integer height;

    private Runnable callbackOnUnexpectedClose;

    public MultiPageInventory(String invTitle){
        pages = new HashMap<>();
        this.invTitle = invTitle;
        currViewingPage = -1;
        callbackOnUnexpectedClose = () -> {};
    }

    /**
     * Does not work with itemamount larger than the maximum itemstack amount of that item.
     * @param items
     */
    public void addItems(MultiPageItem ... items){

        for(MultiPageItem item : items){

            MultiPageItem returnedItems = item;

            for(int pageIndex = 0; returnedItems != null; pageIndex++){
                if(!pages.containsKey(pageIndex)){
                    pages.put(pageIndex, new InventoryPage());
                }
                returnedItems = pages.get(pageIndex).addItem(returnedItems, false);
            }

        }
    }

    public void addItem(MultiPageItem item, Integer page, Integer slot){
        if(!pages.containsKey(page)){
            pages.put(page, new InventoryPage());
        }
        pages.get(page).addItem(item, false, slot);

        fixMissingEmptyPages();
    }

    public void fixMissingEmptyPages(){


        for(int i = 0; i < pages.size(); i++){
            if(!pages.containsKey(i)){
                pages.put(i, new InventoryPage());
            }
        }
    }

    public void setHeight(Integer height){

        if(height > 5){
            //Real max height is 6, but we need one row for the navigation
            throw new IllegalArgumentException("An inventory has a  maximum height of 5, can't set it to " + height);
        }

        this.height = height;
    }

    public void setCallbackOnUnexpectedClose(Runnable callback){
        callbackOnUnexpectedClose = callback;
    }
    protected Runnable getCallbackOnUnexpectedClose(){
        return callbackOnUnexpectedClose;
    }

    /*public void clearUnusedPages(){

        List<InventoryPage> tmp = new ArrayList<>();

        for(Integer index : pages.keySet()){
            if(pages.get(index).getContent().size() == 0){
                pages.remove(index);
            }else{
                tmp.add(pages.get(index));
            }
        }

        pages.clear();

        for(InventoryPage page : tmp){
            pages.put(tmp.indexOf(page), page);
        }

    }*/

    protected Inventory getInventoryRepresentation(Integer index){

        HashMap<Integer, MultiPageItem> content = pages.get(index) != null?pages.get(index).getContent():new HashMap<>();

        Integer absoluteHeight = (content.size()/9 + (content.size()%9 == 0?0:1)) + (pages.size() > 1?1:0);

        Inventory inv =  Bukkit.createInventory(null, absoluteHeight*9, invTitle);//54 is the maximum size, 9 slots needed for navigation


        for(Integer slot : content.keySet()){
            inv.setItem(slot, content.get(slot).getItemStack());
        }

        if(pages.size() > 1){
            byte barColor = 5; //light green
            byte navColor = 13; //green
            byte currColor = 11; //blue

            inv.setItem(45, new ItemStack(Material.WOOL, 1, barColor));
            inv.setItem(46, new ItemStack(Material.WOOL, 1, barColor));
            inv.setItem(47, new ItemStack(Material.WOOL, 1, barColor));

            if(index > 0){
                inv.setItem(48, new ItemStack(Material.WOOL, index, navColor));
            }else{
                inv.setItem(48, new ItemStack(Material.BARRIER, 1));
            }

            inv.setItem(49, new ItemStack(Material.WOOL, index + 1, currColor));

            if(!(index + 1 >= pages.size())){
                inv.setItem(50, new ItemStack(Material.WOOL, index + 2, navColor));
            }else{
                inv.setItem(50, new ItemStack(Material.BARRIER, 1));
            }

            inv.setItem(51, new ItemStack(Material.WOOL, 1, barColor));
            inv.setItem(52, new ItemStack(Material.WOOL, 1, barColor));
            inv.setItem(53, new ItemStack(Material.WOOL, 1, barColor));
        }

        return inv;
    }

    public Boolean isViewedBySomeone(){
        return (currViewingPage != -1);
    }

    protected Integer getViewingPage(){
        return currViewingPage;
    }

    protected void setViewingPage(Integer viewingPage){
        currViewingPage = viewingPage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MultiPageInventory that = (MultiPageInventory) o;

        if (!pages.equals(that.pages)) return false;
        if (!invTitle.equals(that.invTitle)) return false;
        return currViewingPage.equals(that.currViewingPage);
    }

    @Override
    public int hashCode() {
        int result = pages.hashCode();
        result = 31 * result + invTitle.hashCode();
        result = 31 * result + currViewingPage.hashCode();
        return result;
    }
}
