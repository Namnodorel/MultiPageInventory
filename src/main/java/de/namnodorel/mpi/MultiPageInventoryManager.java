package de.namnodorel.mpi;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class MultiPageInventoryManager extends JavaPlugin{

    private static JavaPlugin pluginInstance;
    private static HashMap<String, MultiPageInventory> playersViewingInventory;

    @Override
    public void onEnable(){
        pluginInstance = this;
        playersViewingInventory = new HashMap<>();

        pluginInstance.getServer().getPluginManager().registerEvents(new MultiPageInventoryListener(), pluginInstance);
    }

    public static void showToPlayer(MultiPageInventory inventory, Player p){
        showToPlayer(inventory, p, 0);
    }

    public static void showToPlayer(MultiPageInventory inventory, Player p, Integer page){
        if(inventory.isViewedBySomeone()){
            throw new IllegalArgumentException("The given inventory is still being viewed by a player!");
        }else{
            p.openInventory(inventory.getInventoryRepresentation(page));
            playersViewingInventory.put(p.getName(), inventory);
            inventory.setViewingPage(page);
        }
    }

    public static void hideFromCurrentPlayer(MultiPageInventory inventory){
        inventory.setViewingPage(-1);

        for(String playerName : playersViewingInventory.keySet()){
            if(playersViewingInventory.get(playerName).equals(inventory)){
                playersViewingInventory.remove(playerName);
                pluginInstance.getServer().getPlayer(playerName).closeInventory();
                break;
            }
        }
    }

    protected static JavaPlugin getPluginInstance() {
        return pluginInstance;
    }

    protected static HashMap<String, MultiPageInventory> getPlayersViewingInventory() {
        return playersViewingInventory;
    }
}
