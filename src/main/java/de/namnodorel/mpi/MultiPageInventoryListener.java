package de.namnodorel.mpi;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.PlayerInventory;

public class MultiPageInventoryListener implements Listener {
    @EventHandler(priority = EventPriority.HIGH)
    public void onInventoryClick(InventoryClickEvent e){
        if(MultiPageInventoryManager.getPlayersViewingInventory().containsKey(e.getWhoClicked().getName())){

            MultiPageInventory inventory = MultiPageInventoryManager.getPlayersViewingInventory().get(e.getWhoClicked().getName());
            if(!(e.getClickedInventory() instanceof PlayerInventory)){
                int viewingPage = inventory.getViewingPage();

                if(e.getSlot() >= 45 && e.getSlot() <= 53){
                    e.setCancelled(true);

                    if(e.getSlot() == 48 && e.getClickedInventory().getItem(e.getSlot()).getType() != Material.BARRIER){
                        MultiPageInventoryManager.hideFromCurrentPlayer(inventory);
                        MultiPageInventoryManager.showToPlayer(inventory, (Player)e.getWhoClicked(), viewingPage - 1);
                    }else if(e.getSlot() == 50 && e.getClickedInventory().getItem(e.getSlot()).getType() != Material.BARRIER){
                        MultiPageInventoryManager.hideFromCurrentPlayer(inventory);
                        MultiPageInventoryManager.showToPlayer(inventory, (Player)e.getWhoClicked(), viewingPage + 1);
                    }
                }else if(inventory.pages.get(viewingPage) != null && inventory.pages.get(viewingPage).getContent().get(e.getSlot()) != null && inventory.pages.get(viewingPage).getContent().get(e.getSlot()).getOnClickAction() != null){
                    e.setCancelled(true);
                    inventory.pages.get(viewingPage).getContent().get(e.getSlot()).getOnClickAction().run(e, inventory);
                }
            }else{
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInventoryClose(InventoryCloseEvent e) {

        if(MultiPageInventoryManager.getPlayersViewingInventory().containsKey(e.getPlayer().getName())){
            MultiPageInventoryManager.getPlayersViewingInventory().get(e.getPlayer().getName()).getCallbackOnUnexpectedClose().run();
            MultiPageInventoryManager.hideFromCurrentPlayer(MultiPageInventoryManager.getPlayersViewingInventory().get(e.getPlayer().getName()));
        }

    }
}
