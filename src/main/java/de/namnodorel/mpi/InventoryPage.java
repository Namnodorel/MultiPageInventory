package de.namnodorel.mpi;

import java.util.HashMap;

public class InventoryPage {

    private HashMap<Integer, MultiPageItem> content;

    public InventoryPage(){
        content = new HashMap<>();
    }

    /**
     * Does not work with itemamount larger than the maximum itemstack amount of that item.
     * @param item The item to be added to the page
     * @param addUp Whether or not the given item should add up on existing items of the same type
     * @return Any items that could not be placed. null if everything went well
     */
    public MultiPageItem addItem(MultiPageItem item, Boolean addUp) {

        boolean success = false;

        if(!addUp){

            for(int i = 0; i <= 45; i++){
                if(!content.containsKey(i)){
                    content.put(i, item);
                    success = true;
                    break;
                }
            }
        }else{
            for(Integer index : content.keySet()){

                MultiPageItem existingItem = content.get(index);

                if(existingItem.getItemStack().getType().equals(item.getItemStack().getType()) && item.getItemStack().getAmount() > 0){
                    while(existingItem.getItemStack().getAmount() < existingItem.getItemStack().getMaxStackSize() && item.getItemStack().getAmount() > 0){
                        existingItem.getItemStack().setAmount(existingItem.getItemStack().getAmount() + 1);
                        item.getItemStack().setAmount(item.getItemStack().getAmount() - 1);
                    }
                }
            }

            if(item.getItemStack().getAmount() > 0){
                 return addItem(item, false);
            }
        }

        if(!success){
            return item;
        }else{
            return null;
        }
    }

    /**
     *
     * @param item The item to be added to the page
     * @param addUp Whether or not the given item should add up on existing items of the same type
     * @param index The inventory slot in which the item shall be placed. Must be <= 45
     * @return Any items that could not be placed. null if everything went well
     */
    public MultiPageItem addItem(MultiPageItem item, Boolean addUp, Integer index) {

        if(index > 45){
            return item;
        }

        if(!addUp || !content.containsKey(index) || content.get(index).getItemStack().getType().equals(item.getItemStack().getType())){
            content.put(index, item);
        }else{

            MultiPageItem existingItem = content.get(index);

            while(existingItem.getItemStack().getAmount() < existingItem.getItemStack().getMaxStackSize() && item.getItemStack().getAmount() > 0){
                existingItem.getItemStack().setAmount(existingItem.getItemStack().getAmount() + 1);
                item.getItemStack().setAmount(item.getItemStack().getAmount() - 1);
            }

            if(item.getItemStack().getAmount() > 0){
                return item;
            }
        }

        return null;
    }

    public void clear(){
        content.clear();
    }

    public HashMap<Integer, MultiPageItem> getContent(){
        return content;
    }

}
