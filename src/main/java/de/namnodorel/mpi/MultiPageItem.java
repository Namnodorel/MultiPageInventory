package de.namnodorel.mpi;


import org.bukkit.inventory.ItemStack;

public class MultiPageItem {

    private ItemStack itemStack;
    private InventoryAction onClickAction;

    public MultiPageItem(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public MultiPageItem(ItemStack itemStack, InventoryAction onClickAction) {
        this.itemStack = itemStack;
        this.onClickAction = onClickAction;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public InventoryAction getOnClickAction() {
        return onClickAction;
    }
}
