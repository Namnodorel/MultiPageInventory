package de.namnodorel.mpi;

import org.bukkit.event.inventory.InventoryClickEvent;

@FunctionalInterface
public interface InventoryAction {

    void run(InventoryClickEvent e, MultiPageInventory inventory);

}
