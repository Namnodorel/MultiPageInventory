Example usage:

    MultiPageInventory mpi = new MultiPageInventory("Test");
    mpi.addItems(new MultiPageItem(new ItemStack(Material.DIAMOND, 10)), new MultiPageItem(new ItemStack(Material.IRON_BLOCK, 15)), new MultiPageItem(new ItemStack(Material.IRON_BLOCK, 60)));

    MultiPageItem runnableTest = new MultiPageItem(new ItemStack(Material.APPLE, 27), (e, inventory) -> e.getWhoClicked().sendMessage("§cClick!!!!11211einseinseelf"));

    mpi.addItem(runnableTest, 1, 16);
    mpi.addItem(new MultiPageItem(new ItemStack(Material.WORKBENCH, 50)), 3, 23);
    MultiPageInventoryManager.showToPlayer(mpi, p);